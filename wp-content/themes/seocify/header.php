<?php
/**
 * header.php
 *
 * The header for the theme.
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <meta name="language" content="English">
        <meta name="geo.region" content="India" />
        <meta name="geo.placename" content="Hyderabad" />
        <meta name="robots" content="index, follow" />
        <meta name="google-site-verification=Te1lBjRFS8SLgLAkHa9JASs6zKGPNaDyYwCyrBP-zqQ" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="canonical" href="http://praneethmedia.com/" />
        <!-- Hrflang Tags -->
        <link rel="alternate" href="http://praneethmedia.com/" hreflang="en-us" />
        <link rel="alternate" href="http://praneethmedia.com/" hreflang="en-gb" />
        <link rel="alternate" href="http://praneethmedia.com/" hreflang="en-in" />
<meta name="keywords" content=" best digital marketing agency,digital marketing agency,digital marketing services,best digital marketing company,top digital marketing company,digital marketing in hyderabad,top digital marketing agencies,top digital marketing companies,top digital marketing services,top social media marketing company,top digital media company,best global digital marketing agency ,best seo company,web development company,website development services,website development company,website design and development company,best digital marketing services,web design agency,best web development company,web design services."/>

<meta name="description" content=  "Praneeth Media - best digital marketing company in India. We redefine the digital marketing to deliver the quality results. Switch to professional digital marketing services today."/>
<meta property="og:locale" content="en_US" />
<meta property="og:url" content="http://praneethmedia.com/" />
<meta property="og:site_name" content="Praneeth Media" />
<meta property="og:image" content="-" alt="Praneeth Media">
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
<meta property="og:title" content="Digital Marketing Agency | Praneeth Media" />
<meta property="og:description" content="Praneeth Media - best digital marketing company in India. We redefine the digital marketing to deliver the quality results. Switch to professional digital marketing services today."/>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-R02805DBYW"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-R02805DBYW');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WV7WL7X');</script>
<!-- End Google Tag Manager -->


        
		<?php wp_head(); ?>
		

    </head>

    <body <?php body_class(); ?> data-spy="scroll" data-target="#header">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WV7WL7X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

        
<?php

$is_sticky_header = seocify_option('is_sticky_header');
$is_transparent_header = seocify_option('is_transparent_header');
$show_top_header = seocify_option('show_top_header');
$header_style = seocify_option('header_style');
$header_class = 'header';
if ($is_sticky_header):
    $header_class .= ' nav-sticky xs__sticky_header';
endif;

if ($is_transparent_header):
    $header_class .= ' header-transparent';
endif; 

$top_header_style= 1;

if($header_style <= 2 ){
    $top_header_style = $header_style;
}

?>

    <div class="<?php echo esc_attr($header_class);?>">
        <?php if($show_top_header): get_template_part('template-parts/navigation/nav','top-bar-'.$top_header_style.''); endif; ?>
        <?php get_template_part( 'template-parts/navigation/nav','primary-'.$header_style.'' ); ?>
    </div>