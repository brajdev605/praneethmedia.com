<?php
/**
 * Blog Header
 *
 */
$bg_image				 = $heading				 = $overlay				 = $overlay				 = '';
$page_banner_title		 = $page_banner_subtitle	 = $page_banner_desc		 = $header_image			 = $header_buttons			 = $header_icons			 = '';


if ( defined( 'FW' ) ) {

	$global_page_show_breadcrumb = seocify_option( 'show_breadcrumb' );

	$global_page_banner_img		 = seocify_option( 'page_banner_img' );
	$global_page_banner_title	 = seocify_option( 'page_banner_title' );
	$global_show_page_banner	 = seocify_option( 'show_page_banner' );



	//Page settings
	$page_banner_title	 = fw_get_db_post_option( get_the_ID(), 'header_title' );
	$header_image		 = fw_get_db_post_option( get_the_ID(), 'header_image' );
	$show_breadcrumb			 = fw_get_db_post_option( get_the_ID(), 'show_breadcrumb' );

	if ( isset( $header_image[ 'url' ] ) && $header_image[ 'url' ] != '' ) {
		$bg_image = 'style="background-image: url(' . $header_image[ 'url' ] . ')"';
	} elseif ( $global_page_banner_img != '' ) {
		$bg_image = 'style="background-image: url(' . $global_page_banner_img . ')"';
	}
	if ( $page_banner_title != '' ) {
		$page_banner_title = $page_banner_title;
	} elseif ( $global_page_banner_title != '' ) {
		$page_banner_title = $global_page_banner_title;
	} else {
		$page_banner_title = get_the_title();
	}
	if ( $show_breadcrumb) {
		$page_show_breadcrumb = $show_breadcrumb;
	}else {
		$page_show_breadcrumb = $global_page_show_breadcrumb;
	}
}else {
	$bg_image = 'style="background-image: url(' . SEOCIFY_IMAGES_URI . '/backgrounds/about.png)"';
	$page_banner_title = get_the_title();
	$global_show_page_banner = 'yes';
	$page_show_breadcrumb = 'no';
}

if ( $global_show_page_banner ):
$subTitle = get_field( "sub_title", get_the_ID() );
	?>

	<section class="inner-banner-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="inner-banner-content">
						<h1 class="inner-banner-title">
							<?php echo esc_html( $page_banner_title ); ?>
	                    </h1>
	                    <?php
	                    if($subTitle != ''){
	                    	echo '<p>'. $subTitle .'</p>';
	                    }
	                    ?>
						<?php if ( $page_show_breadcrumb == 'yes' ): ?>
							<?php seocify_get_breadcrumbs(); ?>
						<?php endif; ?>
					</div>
					<?php
					$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
					if($featured_img_url == ''){
						$featured_img_url = '/wp-content/themes/seocify/assets/images/praneethmedia_about.png';
					}
					?>
					<img data-offset="10" src="<?php echo $featured_img_url; ?>" alt="breadcrumb_patterm" class="prl nav-bg-1" style="transform: translate3d(0px, -5px, 0px);">
				</div>
			</div>
	    </div>
		<?php if ( $bg_image ): ?>
			<div class="banner-image" <?php echo wp_kses_post( $bg_image ); ?>></div>
		<?php endif; ?>
	</section>

<?php endif; ?>